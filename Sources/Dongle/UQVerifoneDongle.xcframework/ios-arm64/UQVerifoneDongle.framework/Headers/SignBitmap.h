//
//  SignBitmap.h
//  MUGPOS
//
//  Created by Samgoni Mac on 2021/03/02.
//  Copyright © 2021 Jinseung It Inc. All rights reserved.
//

#ifndef SignBitmap_h
#define SignBitmap_h

#import <UIKit/UIKit.h>

//#define SIGN_WIDTH    128.0
//#define SIGN_HEIGHT    64.0

#define SIGN_WIDTH    256.0
#define SIGN_HEIGHT    110.0

@interface SignBitmap : NSObject
+ (NSData*)createBmpDataByImage:(UIImage*) signImage;
+ (NSData*)createToBmp:(NSData*) bitmapData;
@end
#endif /* SignBitmap_h */
