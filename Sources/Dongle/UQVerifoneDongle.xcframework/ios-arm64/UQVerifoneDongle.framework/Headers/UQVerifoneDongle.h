//
//  UQVerifoneDongle.h
//  UQVerifoneDongle
//
//  Created by Samgoni Mac on 2021/08/18.
//

#import <Foundation/Foundation.h>

#import "SignBitmap.h"
#import "GCDAsyncSocket.h"

//! Project version number for UQVerifoneDongle.
FOUNDATION_EXPORT double UQVerifoneDongleVersionNumber;

//! Project version string for UQVerifoneDongle.
FOUNDATION_EXPORT const unsigned char UQVerifoneDongleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <UQVerifoneDongle/PublicHeader.h>


