//
//  File.swift
//  
//
//  Created by Administrator on 2022/01/25.
//

import Foundation

public class TransactionResponseLotteCreditCard:ResCreditCard {

    var resData:ResCreditCard?
    var tranTraceNum:String = ""

    init?(_ responseString: String) {
        super.init(dictionary: responseString.convertToDictionary() as NSDictionary?)

        if (responseString.convertToDictionary() != nil) {
            resData = ResCreditCard.init(dictionary: responseString.convertToDictionary() as NSDictionary?)
        }
    }


}


