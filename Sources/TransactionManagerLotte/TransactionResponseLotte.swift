//
//  File.swift
//  
//
//  Created by Administrator on 2022/03/30.
//

import Foundation

class TransactionResponseLotte: ITransactionResponse {
    enum TransactionResult {
        case successful
        case failed
    }

    let result: TransactionResult
    let responseString: String
    var responseError:Error?

    init(_ result: TransactionResult,
         _ responseString: String,
         _ error:Error? = nil) {

        self.result = result
        self.responseString = responseString
        self.responseError = error
    }
}
