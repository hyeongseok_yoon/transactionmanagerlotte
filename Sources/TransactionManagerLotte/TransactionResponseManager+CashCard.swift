//
//  File.swift
//  
//
//  Created by Administrator on 2022/03/29.
//

import Foundation
import UQVerifoneDongle
import PaymentCartridgeInterfaces

extension TransactionRequestManagerLotte {

    func updateCashCard(_ payment:IPayment,
                        _ resDongleData:ResKICAccountPay?,
                          paymentCode:PaymentLotteApprovalParameterCode,
                          results: @escaping(Result<TransactionResponseLotteCashCard, PaymentProcessError>) -> Void) {

        print("cash dongle:\(resDongleData?.description)")
        var param:ReqCashCard = makeReqCashCardParam(payment, resDongleData, paymentCode: paymentCode)
        let dataDic:Dictionary = param.dictionaryRepresentation() as! Dictionary<String, Any>
        let serverApi = TransactionRequestController.init(hostUrl!)

        serverApi.sendData(cashCardUrl!, dicData: dataDic) { result in
            if result.result == .successful {
                print("result.responseString \(result.responseString)")
                let (resCd, resMsg) = TransactionManagerLotteUtil.shared.isResStatusCheckCode(jsonString: result.responseString)
                if resCd == "0000" || resCd == "00" {
                    let resData = TransactionResponseLotteCashCard.init(result.responseString)
                    resData!.cardNum = param.body?.icCardNo
                    DispatchQueue.main.async {
                        results(.success(resData!))
                    }
                } else {
                    let err = PaymentProcessError.ServerError(resCd!, resMsg!, result.responseString)
                    DispatchQueue.main.async {
                        results(.failure(err))
                    }
                }
            } else {
                let error = result.responseError! as NSError
                let err = PaymentProcessError.ServerError(String(error.code), error.localizedDescription, result.responseString)

                DispatchQueue.main.async {
                    results(.failure(err))
                }
            }
        }
    }

}
