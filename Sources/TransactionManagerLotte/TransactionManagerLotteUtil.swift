//
//  File.swift
//
//
//  Created by Administrator on 2022/02/25.
//

import Foundation


class TransactionManagerLotteUtil {


    static let shared = TransactionManagerLotteUtil()

    var aToken:String?
    var reToken:String?

    public func invalidTokenCheck(_ resJson:[String: Any]) -> Bool {

        if resJson["resp_code"] != nil {
            if resJson["resp_code"] as! String == "401" {
                print("token error")
                return true
            }
        }
        return false
    }

    public func saveRefreshToken(refToken:String) {
        reToken = refToken
        UserDefaults.standard.set(refToken, forKey: "API_REFRESH_TOKEN")
        UserDefaults.standard.synchronize()
    }

    public func saveAuthToken(authToken:String) {
        aToken = authToken
        UserDefaults.standard.set(aToken, forKey: "API_AUTH_TOKEN")
        UserDefaults.standard.synchronize()
    }

    public func getRefreshToken() -> String {
        UserDefaults.standard.string(forKey: "API_REFRESH_TOKEN") ?? ""
    }

    public func getAuthToken() -> String {
        UserDefaults.standard.string(forKey: "API_AUTH_TOKEN") ?? ""
    }

    //--
    func isResStatusCheckCode(jsonString:String) -> (String?, String?) {

        let vDic = jsonString.convertToDictionary()
        if vDic == nil { return ("-1", "Json error") }
        if (vDic?["body"] != nil) && (vDic?["header"] != nil) {

            let vHeader = vDic?["header"] as! [String : Any]
            let vBody = vDic?["body"] as! [String : Any]

            if vHeader["resp_code"] as! String == "00" {
                return (vBody["resp_code"] as? String, vBody["resp_msg"] as? String)
            } else {
                return (vHeader["resp_code"] as? String, vHeader["resp_msg"] as? String)
            }

        } else {
            if vDic?["resp_code"] != nil {
                return (vDic?["resp_code"] as? String, vDic?["resp_msg"] as? String)
            }
        }

        return ("-1", "server error")
    }


    func strSpace(value:String, maskString:String, isFrontAdd:Bool = false, len:Int) -> String {

        let valStr:NSMutableString = NSMutableString.init(string: value)
        while ( valStr.length < len) {
            if isFrontAdd == false {
                valStr.append(maskString)
            } else {
                valStr.insert(maskString, at: 0)
            }
        }
        return valStr as String
    }


    func newSeqNum() -> Int {

        let userDefKey_LastSeqDay = "lastSeqDay"
        let userDefKey_LastSeqNum = "lastSeqNum"

        let userDefaults = UserDefaults.standard

        if userDefaults.object(forKey: userDefKey_LastSeqDay) == nil ||
            userDefaults.object(forKey: userDefKey_LastSeqNum) == nil {

            userDefaults.set(Date().yyyyMMdd, forKey: userDefKey_LastSeqDay)
            userDefaults.set(Int(1), forKey: userDefKey_LastSeqNum)
            userDefaults.synchronize()
            return Int(1)
        }

        let lastDay:String = userDefaults.object(forKey: userDefKey_LastSeqDay) as! String
        var lastNum:Int = userDefaults.object(forKey: userDefKey_LastSeqNum) as! Int

        if lastDay == Date().yyyyMMdd {
            lastNum = lastNum + 1
            userDefaults.set(lastNum, forKey: userDefKey_LastSeqNum)
            userDefaults.synchronize()
            return lastNum
        } else {

            userDefaults.set(Date().yyyyMMdd, forKey: userDefKey_LastSeqDay)
            userDefaults.set(Int(1), forKey: userDefKey_LastSeqNum)
            userDefaults.synchronize()
            return Int(1)
        }
    }

    func newTranNum() -> Int {

        let userDefKey_LastSeqDay = "lastTranDay"
        let userDefKey_LastSeqNum = "lastTranNum"

        let userDefaults = UserDefaults.standard

        if userDefaults.object(forKey: userDefKey_LastSeqDay) == nil ||
            userDefaults.object(forKey: userDefKey_LastSeqNum) == nil {

            userDefaults.set(Date().yyyyMMdd, forKey: userDefKey_LastSeqDay)
            userDefaults.set(Int(1), forKey: userDefKey_LastSeqNum)
            userDefaults.synchronize()
            return Int(1)
        }

        let lastDay:String = userDefaults.object(forKey: userDefKey_LastSeqDay) as! String
        var lastNum:Int = userDefaults.object(forKey: userDefKey_LastSeqNum) as! Int

        if lastDay == Date().yyyyMMdd {
            lastNum = lastNum + 1
            userDefaults.set(lastNum, forKey: userDefKey_LastSeqNum)
            userDefaults.synchronize()
            return lastNum
        } else {

            userDefaults.set(Date().yyyyMMdd, forKey: userDefKey_LastSeqDay)
            userDefaults.set(Int(1), forKey: userDefKey_LastSeqNum)
            userDefaults.synchronize()
            return Int(1)
        }
    }
}


extension Dictionary {

    var jsonData: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
    }

    func toJSONString() -> String? {
        if let jsonData = jsonData {
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        }

        return nil
    }
}

extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            return try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        }
        return nil
    }
}

//
//extension Date {
//    func formatDate(dateFormat: String, timeZone: TimeZone? = nil) -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = dateFormat
//        if let tz = timeZone {
//            formatter.timeZone = tz
//        }
//        return formatter.string(from: self)
//    }
//
//    var yyyyMMdd: String {
//        return self.formatDate(dateFormat: "yyyyMMdd")
//    }
//
//    var yyyyMMddHHmmss: String {
//        return self.formatDate(dateFormat: "yyyyMMddHHmmss")
//    }
//
//    var logFormat: String {
//        return self.formatDate(dateFormat: "yyyy-MM-dd HH:mm:ss.SSS")
//    }
//
//}
