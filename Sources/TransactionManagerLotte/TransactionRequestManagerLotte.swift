

import Foundation
import PaymentCartridgeInterfaces
import PaymentHandlerInterfaces
import UQVerifoneDongle
import TWError

enum PaymentLotteParameterCardTyeCode:String {
    case credit = "4"
    case union = "1"
    case cash = "2"
}

enum PaymentLotteParameterVanIdCode:String {
    case ksnet = "K"
    case kis = "1"
    case union = "0"
}

enum PaymentLotteApprovalParameterCode:String {
    case approval = "0200"
    case refund = "0420"
    case errorCancel = "0440"
    case void = "0460"
    case forcedApproval = "0580"
}

enum PaymentProcessError: Error {
    case DonggleSignError(DongleResponseCode, String)
    case DonggleExError(DongleResponseCode, String)
    case ServerError(String, String, String)
    case ReTry(String)
}



public final class TransactionRequestManagerLotte {

    var hostUrl:String?
    var creditCardUrl:String?
    var unionCardUrl:String?
    var cashCardUrl:String?
    var tokenUrl:String?

    var storeNum = "0001"
    var posNum = "01"
    var modelNum = ""
    var paramMsgPath = "PS"
    var paramCreditMsgKind = "34"
    var paramCashMsgKind = "32"
    var paramDivMonth = 0


    public init( _ hostServerUrl:String, _ dongleModelNo:String, _ uStoreNo:String, _ uPosNo:String) {

        hostUrl = hostServerUrl
        creditCardUrl = hostServerUrl + "api/v1/creditic"
        unionCardUrl = hostServerUrl + "api/v1/unionpay"
        cashCardUrl = hostServerUrl + "api/v1/cashic"
        tokenUrl = hostServerUrl + ""

        modelNum = dongleModelNo
        storeNum = uStoreNo
        posNum = uPosNo

    }

    func sendCreditCard(_ payment:IPayment,
                        resDongleData:ResDongleEx?,
                        paramCode:PaymentLotteApprovalParameterCode) async throws -> Result<TransactionResponseLotteCreditCard, PaymentProcessError> {

        return try await withCheckedThrowingContinuation { continuation in
            updateCreditCard(payment,
                             resDongleData,
                             paymentCode: paramCode) { res in
                switch res {
                case .success(let resOk):
                    continuation.resume(returning: .success(resOk))
                case .failure(let err):
                    continuation.resume(returning: .failure(err))
                }
            }
        }
    }

    func sendUnionCard(_ payment:IPayment,
                       resDongleData:ResDongleEx?,
                        resPinData:ResJuminPinEx? = nil,
                        paramCode:PaymentLotteApprovalParameterCode) async throws -> Result<TransactionResponseLotteCreditCard, PaymentProcessError>? {

        return try await withCheckedThrowingContinuation { continuation in

            updateUnionCard(payment, resDongleData, resPinData, paymentCode: paramCode) { res in

                switch res {
                case .success(let resOk):
                    continuation.resume(returning: .success(resOk))
                case .failure(let err):
                    continuation.resume(returning: .failure(err))
                }
            }
        }
    }

    func sendCashCard(_ payment:IPayment,
                      resDongleData:ResKICAccountPay?,
                      paramCode:PaymentLotteApprovalParameterCode) async throws -> Result<TransactionResponseLotteCashCard, PaymentProcessError>? {

        return try await withCheckedThrowingContinuation { continuation in
            updateCashCard(payment, resDongleData, paymentCode: paramCode) { res in

                switch res {
                case .success(let resOk):
                    continuation.resume(returning: .success(resOk))
                case .failure(let err):
                    continuation.resume(returning: .failure(err))
                }
            }
        }
    }

}




