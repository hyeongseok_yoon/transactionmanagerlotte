//
//  File.swift
//  
//
//  Created by Administrator on 2022/03/29.
//

import Foundation
import UQVerifoneDongle
import PaymentCartridgeInterfaces

extension TransactionRequestManagerLotte {

    func updateCreditCard(_ payment:IPayment,
                          _ resDongleData:ResDongleEx?,
                          _ resPinData:ResJuminPinEx? = nil,
                          paymentCode:PaymentLotteApprovalParameterCode,
                          results: @escaping(Result<TransactionResponseLotteCreditCard, PaymentProcessError>) -> Void) {

        var param:ReqCreditCard?
        param = makeReqCreditCardParam(payment, resDongleData, paymentCode: paymentCode)

        let dataDic:Dictionary = param!.dictionaryRepresentation() as! Dictionary<String, Any>
        let serverApi = TransactionRequestController.init(hostUrl!)


        serverApi.sendData(creditCardUrl!, dicData: dataDic) { result in
            if result.result == .successful {
                let (resCd, resMsg) = TransactionManagerLotteUtil.shared.isResStatusCheckCode(jsonString: result.responseString)
                if resCd == "0000" || resCd == "00" {
                    let resData = TransactionResponseLotteCreditCard.init(result.responseString)
                    DispatchQueue.main.async {
                        results(.success(resData!))
                    }
                } else {
                    if paymentCode == .errorCancel {
                        DispatchQueue.main.async {
                            let resData = TransactionResponseLotteCreditCard.init(result.responseString)
                            results(.success(resData!))
                        }
                    } else {
                        let err = PaymentProcessError.ServerError(resCd!, resMsg!, result.responseString)
                        DispatchQueue.main.async {
                            results(.failure(err))
                        }
                    }
                }
            } else {
                let error = result.responseError! as NSError
                let err = PaymentProcessError.ServerError(String(error.code), error.localizedDescription, result.responseString)
                if error.code == -1001 { //request timeout
                    payment.processorTransactionId = (param?.body?.eCnClNo)!
                    payment.accountNumber = (param?.body?.cardNo)!
                }
                DispatchQueue.main.async {
                    results(.failure(err))
                }
            }
        }
    }
}
