
import Foundation
import PaymentCartridgeInterfaces
import PaymentHandlerInterfaces
import TWError
import UIKit
import UQVerifoneDongle

public protocol IPaymentManager {

}



public final class TransactionManagerLotte:IPaymentManager {

    var dongleManager:DongleManager
    var srvUrl:String?
    var modelNo:String?
    var storeNo:String?
    var posNo:String?

    public init(_ dongleIp:String, _ donglePort:Int, _ hostUrl:String, _ dongleModelNo:String, _ uStoreNo:String, _ uPosNo:String) {

        srvUrl = hostUrl
        dongleManager = DongleManager.init()
        dongleManager.setupIp(ip: dongleIp, port: UInt16(donglePort) )
        modelNo = dongleModelNo
        storeNo = uStoreNo
        posNo = uPosNo
    }

    public func terminalConnect(_ results: @escaping((Swift.Result<Any, Error>)) -> Void) {

        dongleManager.dongleConnect() { res in
            switch res {
            case .success(let res):
                results(.success(res))
            case .failure(let err):
                results(.failure(err))
            }
        }
    }

    public func terminalReest(_ results: @escaping((Swift.Result<Any, Error>)) -> Void) {

        dongleManager.dongleReset() { res in
            switch res {
            case .success(let res):
                results(.success(res))
            case .failure(let err):
                results(.failure(err))
            }
        }
    }


    func paymentLotteTrasactionErrorHandler(error:Error) -> TWError {


        let error = NSError(domain: "\(-1)::\("err")", code: 503, userInfo: [ NSLocalizedDescriptionKey: "Dongle Error"])
        return TWError.init("[\(error.code)]:\(error.localizedDescription)", error)


//        switch error {
//        case PaymentProcessError.DonggleExError(let code, let msg):
//            let error = NSError(domain: "\(code)::\(msg)", code: 503, userInfo: [ NSLocalizedDescriptionKey: "Dongle Error"])
//            return TWError.init("[\(code)]:\(msg)", error)
//
//        case PaymentProcessError.ServerError(let code, let msg, let jsonString):
//            let error = NSError(domain: "\(code)::\(msg)", code: 501, userInfo: [ NSLocalizedDescriptionKey: jsonString])
//            return TWError.init(jsonString, error)
//
//        default:
//            let error = NSError(domain: "error", code: 500, userInfo: [ NSLocalizedDescriptionKey: "maybe Server Error"])
//            return TWError.init("maybe Server Error", error)
//        }
    }

//--

    public func creditCardScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {

        Task.init {
            do {
                //신용 카드

                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)
                let cardRes = try await dongleManager.dongleCreditCard(amount: payment.amount)
                if payment.amount > 50000 {

                    let signRes = try await dongleManager.dongleSign(amount: payment.amount)
                    if signRes?.bmpImage != nil {
                        payment.signature = UIImage(data: (signRes?.bmpImage)!)?.cgImage
                    }
                }
                let updateRes = try await responseManager.sendCreditCard(payment,
                                                                         resDongleData: cardRes!,
                                                                         paramCode: .approval)

                dongleManager.dongleDisconnect()
                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                }
//                if UserDefaults.standard.value(forKey: "PAYMENT_CREDIT_APPROVAL") as! String == "1" { //appType : "9" 강제 승인
//                    let updateRes = try await responseManager.sendCreditCard(resDongleData: nil,
//                                                                             paramCode: .forcedApproval)
//
//
//                    dongleManager.dongleDisconnect()
//
//                    switch updateRes {
//                    case .success(let resOk):
//                        completion(.success(resOk))
//                    case .failure(let error):
//                        completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
//                    }
//
//                } else {
//                    if payment.amount > 50000 {
//
//                        let signRes = try await dongleManager.dongleSign(amount: payment.amount)
//                        if signRes?.bmpImage != nil {
//                            payment.signature = UIImage(data: (signRes?.bmpImage)!)?.cgImage
//                        }
//                    }
//                    let cardRes = try await dongleManager.dongleCreditCard(amount: payment.amount)
//                    let updateRes = try await responseManager.sendCreditCard(resDongleData: cardRes!,
//                                                                             paramCode: .approval)
//
//                    dongleManager.dongleDisconnect()
//                    switch updateRes {
//                    case .success(let resOk):
//                        completion(.success(resOk))
//                    case .failure(let error):
//                        completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
//                    }
//                }
            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    public func creditCardRefundScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {
        Task.init {
            do {
                //신용 카드
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)
                let dicD:NSDictionary = payment.originalPayment?.terminalId!.convertToDictionary() as! NSDictionary// as! NSDictionary
                let resD:ResCreditCard? = ResCreditCard.init(dictionary: dicD as NSDictionary?)
                if resD?.body?.tranType == "0590" {
                    //강제승인 건
                    let updateRes = try await responseManager.sendCreditCard(payment, resDongleData: nil, paramCode: .refund)

                    dongleManager.dongleDisconnect()

                    switch updateRes {
                    case .success(let resOk):
                        completion(.success(resOk))
                    case .failure(let error):
                        completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                    }


                } else {
                    let cardRes = try await dongleManager.dongleCreditCard(amount: payment.amount)
                    if abs(payment.amount) > 50000 {
                        let signRes = try await dongleManager.dongleSign(amount: abs(payment.amount))
                        if signRes?.bmpImage != nil {
                            payment.signature = UIImage(data: (signRes?.bmpImage)!)?.cgImage
                        }
                    }

                    let updateRes = try await responseManager.sendCreditCard(payment,
                                                                             resDongleData: cardRes!,
                                                                             paramCode: .refund)
                    dongleManager.dongleDisconnect()

                    switch updateRes {
                    case .success(let resOk):
                        completion(.success(resOk))
                    case .failure(let error):
                        completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                    }


                }
            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }



    public func creditCardVoidScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {
        Task.init {
            do {

                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)

                let updateRes = try await responseManager.sendCreditCard(payment,
                                                                         resDongleData: nil,
                                                                         paramCode: .void)
                dongleManager.dongleDisconnect()

                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                }


            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }



    public func creditCardErrorCancelScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {
        Task.init {
            do {

                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)

                let updateRes = try await responseManager.sendCreditCard(payment,
                                                                          resDongleData: nil,
                                                                         paramCode: .errorCancel)
                dongleManager.dongleDisconnect()
                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                }
            } catch {
                dongleManager.dongleDisconnect()
                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    //--
    //Union pay
    public func unionCardScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {
        Task.init {
            do {

                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)


                let pinNumRes = try await dongleManager.donglePinNum()
                let cardRes = try await dongleManager.dongleCreditCard(amount: payment.amount)
                if payment.amount > 50000 {
                    let signRes = try await dongleManager.dongleSign(amount: payment.amount)
                    if signRes?.bmpImage != nil {
                        payment.signature = UIImage(data: (signRes?.bmpImage)!)?.cgImage
                    }
                }
                let updateRes = try await responseManager.sendUnionCard(payment, resDongleData: cardRes,
                                                                         resPinData: pinNumRes,
                                                                         paramCode: .approval)


                dongleManager.dongleDisconnect()

                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print("")
                }


            } catch {
                dongleManager.dongleDisconnect()
                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    public func unionCardRefundScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {
        Task.init {
            do {
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)

                let pinNumRes = try await dongleManager.donglePinNum()
                let cardRes = try await dongleManager.dongleCreditCard(amount: payment.amount)
                if abs(payment.amount) > 50000 {
                    let signRes = try await dongleManager.dongleSign(amount: abs(payment.amount))
                    if signRes?.bmpImage != nil {
                        payment.signature = UIImage(data: (signRes?.bmpImage)!)?.cgImage
                    }
                }
                let updateRes = try await responseManager.sendUnionCard(payment, resDongleData: cardRes,
                                                                        resPinData: pinNumRes,
                                                                         paramCode: .refund)
                dongleManager.dongleDisconnect()

                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print("??")
                }


            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    public func unionCardVoidScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {
        Task.init {
            do {
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)

                let updateRes = try await responseManager.sendUnionCard(payment, resDongleData: nil,
                                                                         paramCode: .void)
                dongleManager.dongleDisconnect()
                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print("??")
                }


            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    public func unionCardErrorCancelScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCreditCard, TWError>)  -> Void) {
        Task.init {
            do {
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)


                let updateRes = try await responseManager.sendUnionCard(payment, resDongleData: nil,
                                                                         paramCode: .errorCancel)
                dongleManager.dongleDisconnect()
                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print(" ")
                }

            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    //--
    //Cash pay
    public func cashCardScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCashCard, TWError>)  -> Void) {
        Task.init {
            do {
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)
                let cardRes = try await dongleManager.dongleCashCard(amount: payment.amount)
                let updateRes = try await responseManager.sendCashCard(payment, resDongleData: cardRes, paramCode: .approval)
                dongleManager.dongleDisconnect()

                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print("")
                }

            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    public func cashCardRefundScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCashCard, TWError>)  -> Void) {
        Task.init {
            do {
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)

                let cardRes = try await dongleManager.dongleCashCard(amount: payment.amount)
                let updateRes = try await responseManager.sendCashCard(payment, resDongleData: cardRes, paramCode: .refund)
//                completion(.success(updateRes!))

                dongleManager.dongleDisconnect()

                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print("??")
                }


            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    public func cashCardVoidScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCashCard, TWError>)  -> Void) {
        Task.init {
            do {
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)

//                let cardRes = try await dongleManager.dongleCashCard(amount: payment.amount)
                let updateRes = try await responseManager.sendCashCard(payment, resDongleData: nil, paramCode: .void)
//                completion(.success(updateRes!))

                dongleManager.dongleDisconnect()

                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print("??")
                }

            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }

    public func cashCardErrorCancelScneario(_ payment:IPayment, completion: @escaping (Result<TransactionResponseLotteCashCard, TWError>)  -> Void) {
        Task.init {
            do {
                let responseManager = TransactionRequestManagerLotte.init(srvUrl!, modelNo!, storeNo!, posNo!)

                let updateRes = try await responseManager.sendCashCard(payment, resDongleData: nil, paramCode: .errorCancel)
//                completion(.success(updateRes!))
                dongleManager.dongleDisconnect()

                switch updateRes {
                case .success(let resOk):
                    completion(.success(resOk))
                case .failure(let error):
                    completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
                case .none:
                    print("??")
                }


            } catch {
                dongleManager.dongleDisconnect()

                completion(.failure(paymentLotteTrasactionErrorHandler(error: error)))
            }
        }
        return
    }


}



extension Date {
    func formatDate(dateFormat: String, timeZone: TimeZone? = nil) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        if let tz = timeZone {
            formatter.timeZone = tz
        }
        return formatter.string(from: self)
    }

    var yyyyMMdd: String {
        return self.formatDate(dateFormat: "yyyyMMdd")
    }

    var yyyyMMddHHmmss: String {
        return self.formatDate(dateFormat: "yyyyMMddHHmmss")
    }

    var logFormat: String {
        return self.formatDate(dateFormat: "yyyy-MM-dd HH:mm:ss.SSS")
    }

}

