//
//  File.swift
//  
//
//  Created by Administrator on 2022/03/29.
//

import Foundation
import UQVerifoneDongle
import PaymentCartridgeInterfaces

extension TransactionRequestManagerLotte {


    enum PaymentLotteCardType:String {

        case credit = "4"
        case cash = "2"
        case union = "1"
    }


    func makeReqCashCardParam( _ payment:IPayment, _ resDongleData:ResKICAccountPay?, paymentCode:PaymentLotteApprovalParameterCode) -> ReqCashCard {

        //고정 헤더
        var param:ReqCashCard  = ReqCashCard.init(dictionary: nil)!
        //변화하지 않는 고정값
        param.header?.msgPath = paramMsgPath
        param.header?.msgKind = paramCashMsgKind
        param.header?.respCode = ""
        param.header?.respMsg = ""
        param.header?.seqNo = Int(payment.merchantTransactionId)
        param.header?.saleDate = Date().yyyyMMdd
        param.body?.itemId = paramCashMsgKind

        //포스에서 받는 값
        param.header?.storeNo = storeNum
        param.header?.posNo = posNum
        param.header?.tranNo = Int(payment.receiptNumber ?? "")
        param.body?.tranType = paymentCode.rawValue
        param.body?.vanCode =  PaymentLotteCardType.cash.rawValue

        if paymentCode == .approval {
            param.body?.saleAmt = NSDecimalNumber(decimal: payment.amount).intValue
            param.body?.issueCd = resDongleData?.issBankCode
            param.body?.track3 = resDongleData?.track3
            param.body?.encInfo = resDongleData?.encInfo
            param.body?.wCC = "I"
            param.body?.icCardNo = resDongleData?.csNumber
            if payment.amount > 50000 { param.body?.simPlify = "00" }
            else { param.body?.simPlify = "01" }
            param.body?.partCnclYn = "0"
            param.body?.tranTraceNo = String(format: "%04d", Int((param.header?.storeNo)!)!) + String(format: "%02d", Int((param.header?.posNo)!)! ) + String(format:"%05d",(param.header?.seqNo)!) + "0" + String(format:"%04d",(param.header?.tranNo)!)

        } else if paymentCode == .refund {
            param.body?.saleAmt = NSDecimalNumber(decimal:abs(payment.amount)).intValue

            if NSDecimalNumber(decimal:abs(payment.amount)).intValue != NSDecimalNumber(decimal:abs(payment.originalPayment!.amount)).intValue {
                param.body?.partCnclYn = "1"
            } else {
                param.body?.partCnclYn = "0"
            }

            param.body?.issueCd = resDongleData?.issBankCode
            param.body?.track3 = resDongleData?.track3
            param.body?.encInfo = resDongleData?.encInfo
            param.body?.wCC = "I"
            param.body?.icCardNo = resDongleData?.csNumber
            if payment.originalPayment!.amount > 50000 { param.body?.simPlify = "00" }
            else { param.body?.simPlify = "01" }

            let dicD:NSDictionary = payment.originalPayment?.terminalId?.convertToDictionary() as! NSDictionary
            let resD:ResCashCard? = ResCashCard.init(dictionary: dicD as NSDictionary?)
            param.body?.orgAppDate = resD?.body?.appDT
            param.body?.orgAppNo = resD?.body?.appNo
        } else if paymentCode == .void {
            param.body?.saleAmt = NSDecimalNumber(decimal: abs(payment.originalPayment!.amount)).intValue
            param.body?.issueCd = "003"
            param.body?.track3 = ""
            param.body?.encInfo = ""
            param.body?.wCC = ""
            param.body?.icCardNo = payment.originalPayment!.accountNumber
            if payment.originalPayment!.amount > 50000 { param.body?.simPlify = "00" }
            else { param.body?.simPlify = "01" }
            let dicD:NSDictionary = payment.originalPayment?.terminalId?.convertToDictionary() as! NSDictionary
            let resD:ResCashCard? = ResCashCard.init(dictionary: dicD as NSDictionary?)
            param.body?.orgAppDate = resD?.body?.appDT
            param.body?.orgAppNo = resD?.body?.appNo
            param.body?.tranTraceNo = resD?.body?.tranTraceNo

        } else if paymentCode == .errorCancel {
//            param = makeReqCreditCardParamVoid(param: param, paymentCode: paymentCode)

            param.header?.seqNo = Int(payment.merchantTransactionId)
            param.body?.tranType = PaymentLotteApprovalParameterCode.errorCancel.rawValue


            param.body?.saleAmt = NSDecimalNumber(decimal: abs(payment.originalPayment!.amount)).intValue
            param.body?.issueCd = "003"
            param.body?.track3 = ""
            param.body?.encInfo = ""
            param.body?.wCC = ""
            param.body?.icCardNo = payment.originalPayment!.accountNumber
            if payment.originalPayment!.amount > 50000 { param.body?.simPlify = "00" }
            else { param.body?.simPlify = "01" }
//            let dicD:NSDictionary = self.payment.originalPayment?.terminalId?.convertToDictionary() as! NSDictionary
//            let resD:ResCashCard? = ResCashCard.init(dictionary: dicD as NSDictionary?)
            param.body?.orgAppDate = ""//resD?.body?.appDT
            param.body?.orgAppNo = ""//resD?.body?.appNo
            param.body?.tranTraceNo = payment.originalPayment!.processorTransactionId


        }

        payment.processorTransactionId = param.body?.tranTraceNo ?? ""
        payment.accountNumber = param.body?.icCardNo ?? ""

        return param

    }
}
