//
//  File.swift
//  
//
//  Created by Administrator on 2022/03/30.
//

import Foundation


public protocol ITransactionResponse: AnyObject {}
//public protocol TransactionResponseLotte: AnyObject {}

class TransactionRequestController:NSObject, URLSessionDelegate {

    var tokenUrl:String?
    var getData:Dictionary<String, Any>?
    var aToken:String?
    var reToken:String?

    public init(_ hostServerUrl:String) {

        tokenUrl = hostServerUrl + "api/v1/get-token"
    }


    func sendData(_ urlAddress:String, dicData:Dictionary<String, Any>, completion: @escaping ((TransactionResponseLotte) -> Void)) {


        let url = URL(string: urlAddress)!
        let request = NSMutableURLRequest(url: url)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue:OperationQueue.main)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dicData, options: .prettyPrinted)
            print("send param:\(String(describing: dicData.toJSONString()!))")

            request.httpMethod = "POST"
            request.httpBody = jsonData
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(TransactionManagerLotteUtil.shared.getAuthToken(), forHTTPHeaderField: "x-auth-token")
//            request.addValue(TransactionManagerLotteUtil.shared.getRefreshToken(), forHTTPHeaderField: "x-refresh-token")

            request.timeoutInterval = 20.1
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                guard let data = data, error == nil else {
                    print(error?.localizedDescription ?? "No data")

                    completion(TransactionResponseLotte.init(.failed, dicData.toJSONString()!, error))
                    return
                }

                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                if let responseJSON = responseJSON as? [String: Any] {
                    print(responseJSON)
                    if TransactionManagerLotteUtil.shared.invalidTokenCheck(responseJSON) == true {
                        self.getTokenApi { authToken, refreshToken in
                            TransactionManagerLotteUtil.shared.saveAuthToken(authToken: authToken ?? "")
                            TransactionManagerLotteUtil.shared.saveRefreshToken(refToken: refreshToken ?? "")
                            self.sendData(urlAddress, dicData: dicData, completion: completion)
                        }
                        return
                    }
                    completion(TransactionResponseLotte.init(.successful, responseJSON.toJSONString() ?? ""))
                } else {
                    if responseJSON == nil {
                        completion(TransactionResponseLotte.init(.failed, "reponse data fail"))
                    } else {
                        completion(TransactionResponseLotte.init(.failed, responseJSON as? String ?? "reponse data fail"))
                    }
                }
            })
            task.resume()
        } catch let e {
            print(e)
            completion(TransactionResponseLotte.init(.failed, "Request Error"))
        }
    }

    func getTokenApi(completion: @escaping ((String?, String?) -> Void)) {

        let url = URL(string: tokenUrl!)!

        let request = NSMutableURLRequest(url: url)
        request.setValue("0002", forHTTPHeaderField: "StoreNo")
        request.setValue("82", forHTTPHeaderField: "PosNo")
        request.setValue("30", forHTTPHeaderField: "AuthType")

        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue:OperationQueue.main)
        do {
            request.httpMethod = "POST"
            request.timeoutInterval = 20
            print("getTokenApi::Send to::\(url)")
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in

                if let httpResponse = response as? HTTPURLResponse {
                    let authToken = httpResponse.allHeaderFields["x-auth-token"] as? String ?? ""
                    let refreshToken = httpResponse.allHeaderFields["x-refresh-token"] as? String ?? ""
                    completion(authToken,refreshToken)
                    return
                }
                completion("","")
            })
            task.resume()
        } catch let e {
            print(e)
            completion("","")
        }
    }
    //--------------------------------------------

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {

        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            let trust = challenge.protectionSpace.serverTrust
            completionHandler(.useCredential, URLCredential(trust: trust!))
            return
        } else {
            challenge.sender?.cancel(challenge)
        }
    }

    //-------------------------------------------------------


}


