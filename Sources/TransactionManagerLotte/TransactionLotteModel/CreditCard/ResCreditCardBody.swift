//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/01.
//

import Foundation

//신용IC Body Response

public class ResCreditCardBody {

    public var itemId: String?             //34 : 신용카드 item
    public var tranType: String?           //0200:승인,  0420:취소,  0440:장애취소
    public var vanCode: String?            //0:Random K:KSNET
    public var respCode: String?           //응답코드
    public var appDate: String?            //승인일시
    public var apprNo: String?              //승인번호
    public var issueName: String?          //발급사 명
    public var purCode: String?            //매입사 코드
    public var regNo: String?              //가맹점 번호
    public var respMsg: String?            //응답 메시지
    public var prepaidYN: String?          //선불카드 여부
    public var prepaidBalance: String?     //선불카드 잔액
    //결제 성공 후 Request 값을 저장해야 하는 변수 (Void, Network cancel 에 사용)

    public var cardNum: String?     //Card no
    public var modelNo: String?     //Model no
    public var tranTraceNo: String?     //Tran trace no

    

    public class func modelsFromDictionaryArray(array:NSArray) -> [ResCreditCardBody]
    {
        var models:[ResCreditCardBody] = []
        for item in array
        {
            models.append(ResCreditCardBody(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {


        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        vanCode = dictionary!["req_van_code"] as? String
        respCode = dictionary!["resp_code"] as? String
        appDate = dictionary!["appr_date"] as? String
        apprNo = dictionary!["appr_no"] as? String
        issueName = dictionary!["issue_cpny_name"] as? String
        purCode = dictionary!["pur_cpny_code"] as? String
        regNo = dictionary!["frnc_store_no"] as? String
        respMsg = dictionary!["resp_msg"] as? String
        prepaidYN = dictionary!["prepaid_card_remain_flag"] as? String
        prepaidBalance = dictionary!["prepaid_card_remain_amt"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
        dictionary.setValue(self.respCode, forKey: "resp_code")
        dictionary.setValue(self.appDate, forKey: "appr_date")
        dictionary.setValue(self.apprNo, forKey: "appr_no")
        dictionary.setValue(self.issueName, forKey: "issue_cpny_name")
        dictionary.setValue(self.purCode, forKey: "pur_cpny_code")
        dictionary.setValue(self.regNo, forKey: "frnc_store_no")
        dictionary.setValue(self.respMsg, forKey: "resp_msg")
        dictionary.setValue(self.prepaidYN, forKey: "prepaid_card_remain_flag")
        dictionary.setValue(self.prepaidBalance, forKey: "prepaid_card_remain_amt")

        return dictionary
    }

}





