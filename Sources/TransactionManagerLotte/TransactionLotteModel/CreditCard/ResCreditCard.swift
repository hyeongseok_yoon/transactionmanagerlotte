//
//  File.swift
//  
//
//  Created by Administrator on 2022/01/25.
//

import Foundation

public class ResCreditCard {

    public var header:PublicHeader?
    public var body: ResCreditCardBody?// = MODEL_IC_CARD_REQ02030.init(dictionary: nil)

    public init?(dictionary: NSDictionary?) {

        header =  PublicHeader.init(dictionary: dictionary?.object(forKey: "header") as? NSDictionary)
        body = ResCreditCardBody.init(dictionary: dictionary?.object(forKey: "body") as? NSDictionary)
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.header?.dictionaryRepresentation(), forKey: "header")
        dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")
        return dictionary
    }
}
