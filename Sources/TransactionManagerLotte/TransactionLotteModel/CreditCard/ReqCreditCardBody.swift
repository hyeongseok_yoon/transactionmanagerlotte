//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/01.
//

import Foundation


//신용카드 Body Request
public class ReqCreditCardBody {


    public var itemId : String?
    public var tranType : String?
    public var vanCode : String?
    public var wcc : String?
    public var divMonth : Int = 0
    public var saleAmt : Int = 0
    public var taxAmt : Int = 0
    public var orgAppDate : String?
    public var orgAppNo : String?
    public var orgSaleAmt : Int = 0

    public var cardGb : String?
    public var pinNo : String?
    public var cardNo : String?
    public var modelNo : String?
    public var track2 : String?

    public var fallBackType : String?
    public var fallBackReason : String?
    public var kSN : String?
    public var emvData : String?
    public var emvLen : String?
    public var eCnClNo : String?




    public class func modelsFromDictionaryArray(array:NSArray) -> [ReqCreditCardBody]
    {
        var models:[ReqCreditCardBody] = []
        for item in array {
            models.append(ReqCreditCardBody(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            itemId = "34"
            tranType = "0200"
            vanCode = "1"
            wcc = "I"
            return
        }
        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        vanCode = dictionary!["req_van_code"] as? String
        wcc = dictionary!["wcc"] as? String
        divMonth = (dictionary!["insta_mms_cnt"] as? Int) ?? 0
        saleAmt = (dictionary!["pay_amt"] as? Int) ?? 0
        taxAmt = (dictionary!["tax_amt"] as? Int) ?? 0
        orgAppDate = dictionary!["org_appr_date"] as? String
        orgAppNo = dictionary!["org_appr_no"] as? String
        orgSaleAmt = (dictionary!["org_appr_amt"] as? Int) ?? 0
        cardGb = dictionary!["card_type_code"] as? String
        cardNo = dictionary!["customer_card_no"] as? String
        modelNo = dictionary!["model_no"] as? String
        track2 = dictionary!["track2"] as? String
        fallBackType = dictionary!["fallback_flag"] as? String
        fallBackReason = dictionary!["fallback_reason"] as? String
        kSN = dictionary!["ksn"] as? String
        emvData = dictionary!["emv_data"] as? String
        emvLen = dictionary!["emv_len"] as? String
        eCnClNo = dictionary!["tran_trace_no"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
        dictionary.setValue(self.wcc, forKey: "wcc")
        dictionary.setValue(self.divMonth, forKey: "insta_mms_cnt")
        dictionary.setValue(self.saleAmt, forKey: "pay_amt")
        dictionary.setValue(self.taxAmt, forKey: "tax_amt")
        dictionary.setValue(self.orgAppDate, forKey: "org_appr_date")
        dictionary.setValue(self.orgAppNo, forKey: "org_appr_no")
        dictionary.setValue(self.orgSaleAmt, forKey: "org_appr_amt")
        dictionary.setValue(self.cardGb, forKey: "card_type_code")
        dictionary.setValue(self.pinNo, forKey: "union_card_info")
        dictionary.setValue(self.cardNo, forKey: "customer_card_no")
        dictionary.setValue(self.modelNo, forKey: "model_no")
        dictionary.setValue(self.track2, forKey: "track2")
        dictionary.setValue(self.fallBackType, forKey: "fallback_flag")
        dictionary.setValue(self.fallBackReason, forKey: "fallback_reason")
        dictionary.setValue(self.kSN, forKey: "ksn")
        dictionary.setValue(self.emvData, forKey: "emv_data")
        dictionary.setValue(self.emvLen, forKey: "emv_len")
        dictionary.setValue(self.eCnClNo, forKey: "tran_trace_no")
        
        
        return dictionary
    }
}







