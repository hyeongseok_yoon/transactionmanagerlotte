//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/16.
//

import Foundation

public class ReqCreditCard {

    public var header:PublicHeader?
    public var body: ReqCreditCardBody?


    public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            header = PublicHeader.init(dictionary: nil)
            body = ReqCreditCardBody.init(dictionary: nil)
        } else {
            header =  PublicHeader.init(dictionary: dictionary?.object(forKey: "header") as? NSDictionary)
            body = ReqCreditCardBody.init(dictionary: dictionary?.object(forKey: "body") as? NSDictionary)
        }
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.header?.dictionaryRepresentation(), forKey: "header")
        dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")
        return dictionary
    }
}


//
//public func resCheck(checkString:String) -> (Bool, String?, String?) {
//
//    let dic = checkString.convertToDictionary() as NSDictionary?
//
//    guard let hDic = dic?.value(forKey: "header") as? NSDictionary else {
//        //header nil
//        return (false, "-1", "Header is null")
//    }
//
//    if hDic.value(forKey: "RespCode") as? String != "00" {
//        return (false, hDic.value(forKey: "RespCode") as? String, hDic.value(forKey: "RespMsg") as? String)
//    }
//
//    guard let bDic = dic?.value(forKey: "Body") as? NSDictionary else {
//        //body nil
//        return (false, "-2", "Body is null")
//    }
//    if bDic.value(forKey: "RespCode") as? String != "0000" {
//        return (false, bDic.value(forKey: "RespCode") as? String, bDic.value(forKey: "RespMsg") as? String)
//    }
//
//
//    return (true, nil, nil)
//
//}


