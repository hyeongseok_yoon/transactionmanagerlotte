//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/12.
//

import Foundation


public class ReqKeyDownKSNBody {

    var itemId:String?               //ITEM ID        37
    var tranType:String?             //거래구분          KSN KEY 다운로드요청 :500
    var vanCode:String?              //요청 VAN사
    var ecFlag:String?              //요청타입          “EC”: 상호인증
    var keyExchngType:String?        //키교환 구분
    var publicKeyVer:String?         //공개키 버젼


    public class func modelsFromDictionaryArray(array:NSArray) -> [ReqKeyDownKSNBody]
    {
        var models:[ReqKeyDownKSNBody] = []
        for item in array
        {
            models.append(ReqKeyDownKSNBody(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {


        if dictionary == nil {
            itemId = "37"
            tranType = "0500"
            vanCode = "1"
            ecFlag = "EC"
            keyExchngType = "N"
            publicKeyVer = "09"
            return
        }

        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        vanCode = dictionary!["req_van_code"] as? String
        ecFlag = dictionary!["req_ec_flag"] as? String
        keyExchngType = dictionary!["key_exchange_type"] as? String
        publicKeyVer = dictionary!["public_key_ver"] as? String
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
        dictionary.setValue(self.ecFlag, forKey: "req_ec_flag")
        dictionary.setValue(self.keyExchngType, forKey: "key_exchange_type")
        dictionary.setValue(self.publicKeyVer, forKey: "public_key_ver")

        return dictionary
    }

}

