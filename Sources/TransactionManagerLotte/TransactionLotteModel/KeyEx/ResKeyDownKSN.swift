//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/16.
//

import Foundation

public class ResKeyDownKSN {

    public var header:PublicHeader?
    public var body:ResKeyDownKSNBody?

    public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            header =  PublicHeader.init(dictionary: nil)
            body = ResKeyDownKSNBody.init(dictionary: nil)
        } else {
            header = PublicHeader.init(dictionary: (dictionary?.object(forKey: "header") as! NSDictionary))
            body = ResKeyDownKSNBody.init(dictionary: (dictionary?.object(forKey: "body") as! NSDictionary))
        }
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.header?.dictionaryRepresentation(), forKey: "header")
        dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")

        return dictionary
    }
}

