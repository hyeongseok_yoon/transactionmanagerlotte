//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/16.
//

import Foundation


public class ResKeyDownIpek {

    public var header:PublicHeader?
    public var body:ResKeyDownIpekBody?

    public init?(dictionary: NSDictionary?) {

//        let hdStr:String = dictionary?.object(forKey: "Header") as? String ?? ""
//        let bdStr:String = dictionary?.object(forKey: "Body") as? String ?? ""

        header =  PublicHeader.init(dictionary: (dictionary?.object(forKey: "header") as! NSDictionary))
        body = ResKeyDownIpekBody.init(dictionary: (dictionary?.object(forKey: "body") as! NSDictionary))
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.header?.dictionaryRepresentation(), forKey: "Header")
        dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")

        return dictionary
    }

}



