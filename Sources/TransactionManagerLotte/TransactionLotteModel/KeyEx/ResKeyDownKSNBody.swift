//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/12.
//

import Foundation

public class ResKeyDownKSNBody {

    var itemId:String?              // ITEM ID           37
    var tranType:String?            // 거래구분             KSN KEY 다운로드응답 :510
    var vanCode:String?             // 요청 VAN사
    var vanName:String?             // 처리 VAN사명
    var reqGb:String?               // 요청구분              “EC”: 상호인증
    var respCode:String?            // 응답코드
    var respMsg:String?             // 응답메시지
    var respDate:String?              // 응답메시지
    var publicKeyVer:String?        // 공개키 버젼
    var svrRandData:String?         // 거래일련번호-랜덤값
    var hashData:String?            // HashData            SvrRandData 값을 Hash(SHA-256)로 처리한 값
    var signData:String?            // SignData            HashData 값을 개인키로 RSA 암호화한 값


    public class func modelsFromDictionaryArray(array:NSArray) -> [ResKeyDownKSNBody]
    {
        var models:[ResKeyDownKSNBody] = []
        for item in array
        {
            models.append(ResKeyDownKSNBody(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {

        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        vanCode = dictionary!["req_van_code"] as? String
        vanName = dictionary!["req_van_name"] as? String
        reqGb = dictionary!["req_ec_flag"] as? String
        respCode = dictionary!["resp_code"] as? String
        respMsg = dictionary!["resp_msg"] as? String
        respDate = dictionary!["resp_date"] as? String
        publicKeyVer = dictionary!["public_key_ver"] as? String
        svrRandData = dictionary!["random_value"] as? String
        hashData = dictionary!["hash_value"] as? String
        signData = dictionary!["sign_value"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
        dictionary.setValue(self.vanName, forKey: "req_van_name")
        dictionary.setValue(self.reqGb, forKey: "req_ec_flag")
        dictionary.setValue(self.respCode, forKey: "resp_code")
        dictionary.setValue(self.respMsg, forKey: "resp_msg")
        dictionary.setValue(self.respDate, forKey: "resp_date")
        dictionary.setValue(self.publicKeyVer, forKey: "public_key_ver")
        dictionary.setValue(self.svrRandData, forKey: "random_value")
        dictionary.setValue(self.hashData, forKey: "hash_value")
        dictionary.setValue(self.signData, forKey: "sign_value")

        return dictionary
    }
}
