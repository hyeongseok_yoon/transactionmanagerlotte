//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/16.
//

import Foundation



public class ReqKeyDownIpekBody {

    var itemId:String?                //ITEM ID       37
    var tranType:String?              //거래구분        KSN KEY IPEK :520
    var vanCode:String?               //요청 VAN사
//    var reqType:String?               //요청구분        “EC”: 상호인증
    var reqGb:String?               //요청구분        “EC”: 상호인증
    var publicKeyVer:String?          //공개키 버젼
    var ksn:String?                   //키교환 구분
    var indctEncKey:String?           //암호화유도키



    public class func modelsFromDictionaryArray(array:NSArray) -> [ReqKeyDownIpekBody]
    {
        var models:[ReqKeyDownIpekBody] = []
        for item in array
        {
            models.append(ReqKeyDownIpekBody(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            itemId = "37"
            tranType = "0520"
            vanCode = "1"
//            reqType = "EK"
            reqGb = "EC"
            publicKeyVer = " 9"
            ksn = ""
            indctEncKey = ""

            return
        }

        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        vanCode = dictionary!["req_van_code"] as? String
//        reqType = dictionary!["ReqType"] as? String
        reqGb = dictionary!["req_ec_flag"] as? String
        publicKeyVer = dictionary!["public_key_ver"] as? String
        ksn = dictionary!["ksn"] as? String
        indctEncKey = dictionary!["enc_induce_key"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
//        dictionary.setValue(self.reqType, forKey: "ReqType")
        dictionary.setValue(self.reqGb, forKey: "req_ec_flag")
        dictionary.setValue(self.publicKeyVer, forKey: "public_key_ver")
        dictionary.setValue(self.ksn, forKey: "ksn")
        dictionary.setValue(self.indctEncKey, forKey: "enc_induce_key")

        return dictionary
    }

}
