//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/15.
//

import Foundation

public class ResKeyDownIpekBody {

    var itemId:String?              // ITEM ID           37
    var tranType:String?            // 거래구분             KSN KEY 다운로드응답 :510
    var vanCode:String?             // 요청 VAN사
    var vanName:String?             // 처리 VAN사명
    var reqGb:String?               // 요청구분              “EC”: 상호인증
    var respCode:String?            // 응답코드
    var respMsg:String?             // 응답메시지
//    var respDate:String?              // 응답메시지

    var keyCreatDate:String?
    var encDataLen:String?
    var encData:String?


    public class func modelsFromDictionaryArray(array:NSArray) -> [ResKeyDownIpekBody]
    {
        var models:[ResKeyDownIpekBody] = []
        for item in array
        {
            models.append(ResKeyDownIpekBody(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {

        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        vanCode = dictionary!["req_van_code"] as? String
        vanName = dictionary!["req_van_name"] as? String
        reqGb = dictionary!["req_ec_flag"] as? String
        respCode = dictionary!["resp_code"] as? String
        respMsg = dictionary!["resp_msg"] as? String
//        respDate = dictionary!["RespDate"] as? String

        keyCreatDate = dictionary!["key_gen_time"] as? String
        encDataLen = dictionary!["enc_data_len"] as? String
        encData = dictionary!["enc_data"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
        dictionary.setValue(self.vanName, forKey: "req_van_name")
        dictionary.setValue(self.reqGb, forKey: "req_ec_flag")
        dictionary.setValue(self.respCode, forKey: "resp_code")
        dictionary.setValue(self.respMsg, forKey: "resp_msg")
//        dictionary.setValue(self.respDate, forKey: "RespDate")
        dictionary.setValue(self.keyCreatDate, forKey: "key_gen_time")
        dictionary.setValue(self.encDataLen, forKey: "enc_data_len")
        dictionary.setValue(self.encData, forKey: "enc_data")


        return dictionary
    }

}




