//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/29.
//

import Foundation

public class ReqUnionCard {

    public var header:PublicHeader?
    public var body:ReqUnionBody?

    public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            header = PublicHeader.init(dictionary: nil)
            header?.msgKind = "32"
            body = ReqUnionBody.init(dictionary: nil)
        } else {

//            let hdStr:String = dictionary?.object(forKey: "Header") as? String ?? ""
//            let bdStr:String = dictionary?.object(forKey: "Body") as? String ?? ""
//            print("hdStr \()")
//            print("bdStr \()")
//            header =  PublicHeader.init(dictionary: hdStr.convertToDictionary() as? NSDictionary)
            //PublicHeader.init(dictionary: dictionary?.object(forKey: "Header") as? NSDictionary)
//            body = bdStr.convertToDictionary()//REQ_P034_CreditCard_Body.init(dictionary: dictionary?.object(forKey: "Body") as? NSDictionary)
        }
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.header?.dictionaryRepresentation(), forKey: "header")
        dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")

        return dictionary
    }

}




