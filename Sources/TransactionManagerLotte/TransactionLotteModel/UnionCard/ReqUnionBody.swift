//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/05.
//

import Foundation

public class ReqUnionBody {

    public var itemId : String?
    public var tranType : String?
    public var vanCode : String?
    public var wcc : String?
    public var divMonth : Int = 0
    public var saleAmt : Int = 0
    public var taxAmt : Int = 0
    public var orgAppDate : String?
    public var orgAppNo : String?
    public var orgSaleAmt : Int = 0

    public var cardGb : String?
    public var pinNo : String?
    public var cardNo : String?
    public var modelNo : String?
    public var track2 : String?

    public var fallBackType : String?
    public var fallBackReason : String?
    public var kSN : String?
    public var emvData : String?
    public var emvLen : String?
    public var eCnClNo : String?



    public class func modelsFromDictionaryArray(array:NSArray) -> [ReqUnionBody]
    {
        var models:[ReqUnionBody] = []
        for item in array
        {
            models.append(ReqUnionBody(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            return
        }

//        itemId = dictionary!["ItemId"] as? String
//        tranType = dictionary!["TranType"] as? String
////        vanCode = dictionary!["VanCode"] as? String
//        wCC = dictionary!["WCC"] as? String
//        icCardNo = dictionary!["IcCardNo"] as? String
//        tranUniqueNo = dictionary!["TranUniqueNo"] as? String
//        saleAmt = dictionary!["SaleAmt"] as? String
//        vatAmt = dictionary!["VatAmt"] as? String
//        orgAppDate = dictionary!["OrgAppDate"] as? String
//        orgAppNo = dictionary!["OrgAppNo"] as? String
//        issueCd = dictionary!["IssueCd"] as? String
//        track3 = dictionary!["Track3"] as? String
//        encInfo = dictionary!["EncInfo"] as? String
//        simPlify = dictionary!["SimPlify"] as? String
////        cardGbn = dictionary!["CardGbn"] as? String
//        partCnclYn = dictionary!["PartCnclYn"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

//        dictionary.setValue(self.itemId, forKey: "ItemId")
//        dictionary.setValue(self.tranType, forKey: "TranType")
////        dictionary.setValue(self.vanCode, forKey: "VanCode")
//        dictionary.setValue(self.wCC, forKey: "WCC")
//        dictionary.setValue(self.icCardNo, forKey: "IcCardNo")
//        dictionary.setValue(self.saleAmt, forKey: "SaleAmt")
//        dictionary.setValue(self.tranUniqueNo, forKey: "TranUniqueNo")
//        dictionary.setValue(self.vatAmt, forKey: "VatAmt")
//        dictionary.setValue(self.orgAppDate, forKey: "OrgAppDate")
//        dictionary.setValue(self.orgAppNo, forKey: "OrgAppNo")
//        dictionary.setValue(self.issueCd, forKey: "IssueCd")
//        dictionary.setValue(self.track3, forKey: "Track3")
//        dictionary.setValue(self.encInfo, forKey: "EncInfo")
//        dictionary.setValue(self.simPlify, forKey: "SimPlify")
////        dictionary.setValue(self.cardGbn, forKey: "CardGbn")
//        dictionary.setValue(self.partCnclYn, forKey: "PartCnclYn")

        return dictionary
    }
}

