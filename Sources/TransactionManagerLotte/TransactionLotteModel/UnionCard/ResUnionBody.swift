//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/05.
//

import Foundation

public class ResUnionBody {

    public var itemId:String?           //ITEM ID       // 32
    public var tranType:String?         //거래구분        // 0210:승인, 0430:취소
//    public var vanCode:String?          //처리 VAN사
//    public var vanName:String?          //처리 VAN사 명
    public var rspCode:String?          //응답코드        // 정상:0000
    public var appDT:String?            //승인일시
    public var appNo:String?            //승인번호
    public var tranUqNo:String?         //서버에서 생성 후 회신
    public var issueName:String?        //발급기관명
//    public var issueStoreCd:String?     //발급사점별코드
    public var purCode:String?          //매입기관대표코드
//    public var purName:String?          //매입기관명
//    public var purStoreCd:String?       //매입기관점별코드
//    public var chargeRate:String?       //수수료율
//    public var storeChargeAmt:String?   //가맹점수수료
//    public var issueChargeAmt:String?   //발급기관수수료
//    public var purChargeAmt:String?     //매입기관수수료
    public var accountNo:String?        //출력용출금계좌
//    public var regNo:String?            //가맹점관리번호
    public var rspMesg:String?          //기관지정에러메세지

    required public init?(dictionary: NSDictionary?) {

        itemId = dictionary!["ItemId"] as? String
        tranType = dictionary!["TranType"] as? String
//        vanCode = dictionary!["VanCode"] as? String
//        vanName = dictionary!["VanName"] as? String
        rspCode = dictionary!["RspCode"] as? String
        appDT = dictionary!["AppDT"] as? String
        appNo = dictionary!["AppNo"] as? String
        tranUqNo = dictionary!["TranUqNo"] as? String
        issueName = dictionary!["IssueName"] as? String
//        issueStoreCd = dictionary!["IssueStoreCd"] as? String
        purCode = dictionary!["PurCode"] as? String
//        purName = dictionary!["PurName"] as? String
//        purStoreCd = dictionary!["PurStoreCd"] as? String
//        chargeRate = dictionary!["chargeRate"] as? String
//        storeChargeAmt = dictionary!["StoreChargeAmt"] as? String
//        issueChargeAmt = dictionary!["IssueChargeAmt"] as? String
//        purChargeAmt = dictionary!["PurChargeAmt"] as? String
        accountNo = dictionary!["AccountNo"] as? String
//        regNo = dictionary!["RegNo"] as? String
        rspMesg = dictionary!["RspMesg"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        var dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "ItemId")
        dictionary.setValue(self.tranType, forKey: "TranType")
//        dictionary.setValue(self.vanCode, forKey: "VanCode")
//        dictionary.setValue(self.vanName, forKey: "VanName")
        dictionary.setValue(self.rspCode, forKey: "RspCode")
        dictionary.setValue(self.appDT, forKey: "appDT")
        dictionary.setValue(self.appNo, forKey: "AppNo")
        dictionary.setValue(self.tranUqNo, forKey: "TranUqNo")
        dictionary.setValue(self.issueName, forKey: "IssueName")
//        dictionary.setValue(self.issueStoreCd, forKey: "IssueStoreCd")
        dictionary.setValue(self.purCode, forKey: "purCode")
//        dictionary.setValue(self.purName, forKey: "PurName")
//        dictionary.setValue(self.purStoreCd, forKey: "purStoreCd")
//        dictionary.setValue(self.chargeRate, forKey: "ChargeRate")
//        dictionary.setValue(self.storeChargeAmt, forKey: "StoreChargeAmt")
//        dictionary.setValue(self.issueChargeAmt, forKey: "IssueChargeAmt")
//        dictionary.setValue(self.purChargeAmt, forKey: "PurChargeAmt")
        dictionary.setValue(self.accountNo, forKey: "AccountNo")
//        dictionary.setValue(self.regNo, forKey: "RegNo")
        dictionary.setValue(self.rspMesg, forKey: "RspMesg")

        return dictionary
    }

}

