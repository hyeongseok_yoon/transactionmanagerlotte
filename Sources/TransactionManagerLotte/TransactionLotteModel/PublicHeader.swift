//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/01.
//

import Foundation

//공통 헤더
public class PublicHeader {

    public var msgPath : String?
    public var msgKind : String?
    public var seqNo : Int?
    public var saleDate : String?
    public var storeNo : String?
    public var posNo : String?
    public var tranNo : Int?
    public var respMsg : String?
    public var respCode : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [PublicHeader]
    {
        var models:[PublicHeader] = []
        for item in array
        {
            models.append(PublicHeader(dictionary: (item as! NSDictionary))!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            return
        }

        msgPath = dictionary!["msg_path"] as? String
        respMsg = dictionary!["resp_msg"] as? String
        respCode = dictionary!["resp_code"] as? String
        msgKind = dictionary!["msg_kind"] as? String
        seqNo = dictionary!["seq_no"] as? Int
        saleDate = dictionary!["sale_date"] as? String
        storeNo = dictionary!["store_no"] as? String
        posNo = dictionary!["pos_no"] as? String
        tranNo = dictionary!["tran_no"] as? Int
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.msgPath, forKey: "msg_path")
        dictionary.setValue(self.msgKind, forKey: "msg_kind")
        dictionary.setValue(self.seqNo, forKey: "seq_no")
        dictionary.setValue(self.saleDate, forKey: "sale_date")
        dictionary.setValue(self.storeNo, forKey: "store_no")
        dictionary.setValue(self.posNo, forKey: "pos_no")
        dictionary.setValue(self.tranNo, forKey: "tran_no")
        dictionary.setValue(self.respMsg, forKey: "resp_msg")
        dictionary.setValue(self.respCode, forKey: "resp_code")

        return dictionary
    }
}

