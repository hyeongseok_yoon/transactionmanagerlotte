

import Foundation

public class ResCashIcBody {

    public var itemId:String?           //ITEM ID       // 32
    public var tranType:String?         //거래구분        // 0210:승인, 0430:취소
    public var vanCode:String?          //처리 VAN사
    public var rspCode:String?          //응답코드        // 정상:0000
    public var appDT:String?            //승인일시
    public var appNo:String?            //승인번호
    public var tranTraceNo:String?         //서버에서 생성 후 회신
    public var issueName:String?        //발급기관명
    public var purCode:String?          //매입기관대표코드
    public var accountNo:String?        //출력용출금계좌
    public var regNo:String?            //가맹점관리번호
    public var rspMesg:String?          //기관지정에러메세지

    required public init?(dictionary: NSDictionary?) {

        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        vanCode = dictionary!["req_van_code"] as? String
        rspCode = dictionary!["resp_code"] as? String
        appDT = dictionary!["appr_date"] as? String
        appNo = dictionary!["appr_no"] as? String
        tranTraceNo = dictionary!["tran_trace_no"] as? String
        issueName = dictionary!["issue_cpny_name"] as? String
        purCode = dictionary!["pur_cpny_code"] as? String
        accountNo = dictionary!["account_no"] as? String
        regNo = dictionary!["frnc_store_no"] as? String
        rspMesg = dictionary!["RspMesg"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        var dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
        dictionary.setValue(self.rspCode, forKey: "resp_code")
        dictionary.setValue(self.appDT, forKey: "appr_date")
        dictionary.setValue(self.appNo, forKey: "appr_no")
        dictionary.setValue(self.tranTraceNo, forKey: "tran_trace_no")
        dictionary.setValue(self.issueName, forKey: "issue_cpny_name")
        dictionary.setValue(self.purCode, forKey: "pur_cpny_code")
        dictionary.setValue(self.accountNo, forKey: "account_no")
        dictionary.setValue(self.regNo, forKey: "frnc_store_no")
        dictionary.setValue(self.rspMesg, forKey: "resp_msg")

        return dictionary
    }

}

