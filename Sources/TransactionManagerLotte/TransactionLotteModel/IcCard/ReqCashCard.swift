//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/29.
//

import Foundation

public class ReqCashCard {

    public var header:PublicHeader?
    public var body:ReqCashIcBody?
    public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            header = PublicHeader.init(dictionary: nil)
            body = ReqCashIcBody.init(dictionary: nil)
        } else {
            let hdStr:String = dictionary?.object(forKey: "header") as? String ?? ""
            let bdStr:String = dictionary?.object(forKey: "body") as? String ?? ""
            header =  PublicHeader.init(dictionary: hdStr.convertToDictionary() as? NSDictionary)
            PublicHeader.init(dictionary: dictionary?.object(forKey: "header") as? NSDictionary)
        }
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.header?.dictionaryRepresentation(), forKey: "header")
        dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")
        return dictionary
    }

}




