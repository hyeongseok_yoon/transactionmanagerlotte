//
//  File.swift
//  
//
//  Created by Administrator on 2021/11/05.
//

import Foundation

public class ReqCashIcBody {

    public var itemId:String? = ""         // ITEM ID / 32
    public var tranType:String? = ""       //거래구분/0200:승인, 0420:취소, 0440:장애취소, 0460:VOID
    public var vanCode:String? = "2"       //요청 VAN사/2: KOVAN
    public var wCC:String? = ""            //입력구분   /@:수입력, A:Swipe, B:Barcode, I:IC
    public var icCardNo:String? = ""       //카드 데이터 /　from ic reader , 취소시 빈값 전송
    public var tranTraceNo:String? = ""       //거래고유번호/취소시 원거래고유번호 (승인시 빈값)
    public var saleAmt:Int = 0        //결재 금액 /
    public var vatAmt:Int = 0         //부가가치세 /
    public var orgAppDate:String? = ""     //원승인일자/(취소시)
    public var orgAppNo:String? = ""       //원승인번호 /(취소시)
    public var issueCd:String? = ""        //발급기관 대표코드  /　from ic reader
    public var track3:String? = ""         //Track III 정보 /　from ic reader , 취소시 빈값 전송
    public var encInfo:String? = ""        //암호화 정보  /　from ic reader, 취소시 빈값 전송
    public var simPlify:String? = ""       //결제간소처리여부 /　’01’ :간소화 ‘00’ : 일반
    public var partCnclYn:String? = ""     //부분반품여부/전체 반품이 아닌 부분 반품일 경우 1을 넣어줌 [기본 0  부분 반품일 경우만1]]


    required public init?(dictionary: NSDictionary?) {

        if dictionary == nil {
            return
        }

        itemId = dictionary!["item_id"] as? String
        tranType = dictionary!["tran_type"] as? String
        wCC = dictionary!["wcc"] as? String
        vanCode = dictionary!["req_van_code"] as? String
        icCardNo = dictionary!["customer_card_no"] as? String
        tranTraceNo = dictionary!["tran_trace_no"] as? String
        saleAmt = dictionary!["pay_amt"] as? Int ?? 0
        vatAmt = dictionary!["vat_amt"] as? Int ?? 0
        orgAppDate = dictionary!["org_appr_date"] as? String
        orgAppNo = dictionary!["org_appr_no"] as? String
        issueCd = dictionary!["issue_cpny_code"] as? String
        track3 = dictionary!["track3"] as? String
        encInfo = dictionary!["enc_info"] as? String
        simPlify = dictionary!["simple_pay_flag"] as? String
        partCnclYn = dictionary!["return_partial_flag"] as? String

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.itemId, forKey: "item_id")
        dictionary.setValue(self.tranType, forKey: "tran_type")
        dictionary.setValue(self.vanCode, forKey: "req_van_code")
        dictionary.setValue(self.wCC, forKey: "wcc")
        dictionary.setValue(self.icCardNo, forKey: "customer_card_no")
        dictionary.setValue(self.saleAmt, forKey: "pay_amt")
        dictionary.setValue(self.tranTraceNo, forKey: "tran_trace_no")
        dictionary.setValue(self.vatAmt, forKey: "vat_amt")
        dictionary.setValue(self.orgAppDate, forKey: "org_appr_date")
        dictionary.setValue(self.orgAppNo, forKey: "org_appr_no")
        dictionary.setValue(self.issueCd, forKey: "issue_cpny_code")
        dictionary.setValue(self.track3, forKey: "track3")
        dictionary.setValue(self.encInfo, forKey: "enc_info")
        dictionary.setValue(self.simPlify, forKey: "simple_pay_flag")
        dictionary.setValue(self.partCnclYn, forKey: "return_partial_flag")

        return dictionary
    }
}

