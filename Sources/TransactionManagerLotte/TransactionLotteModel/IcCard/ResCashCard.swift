
import Foundation

public class ResCashCard {

    public var header:PublicHeader?
    public var body:ResCashIcBody?
    public init?(dictionary: NSDictionary?) {

        header =  PublicHeader.init(dictionary: dictionary?.object(forKey: "header") as? NSDictionary)
        body = ResCashIcBody.init(dictionary: dictionary?.object(forKey: "body") as? NSDictionary)

    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.header?.dictionaryRepresentation(), forKey: "header")
        dictionary.setValue(self.body?.dictionaryRepresentation(), forKey: "body")
        return dictionary
    }

}

