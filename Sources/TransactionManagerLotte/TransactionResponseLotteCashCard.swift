//
//  File.swift
//  
//
//  Created by Administrator on 2022/03/29.
//

import Foundation

public class TransactionResponseLotteCashCard:ResCashCard {

    var resData:ResCashCard?
    var cardNum:String?

    init?(_ responseString: String) {
        super.init(dictionary: responseString.convertToDictionary() as NSDictionary?)

        if (responseString.convertToDictionary() != nil) {
            resData = ResCashCard.init(dictionary: responseString.convertToDictionary() as NSDictionary?)
        }

    }

}
