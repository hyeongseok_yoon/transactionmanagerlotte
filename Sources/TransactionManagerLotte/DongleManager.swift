//
//  File.swift
//
//
//  Created by Administrator on 2022/01/25.
//

import Foundation
import UQVerifoneDongle

typealias PaymentTransactionSign = (ResSignEx?, Error?) -> Void
typealias PaymentTransactionEx = (ResDongleEx?, Error?) -> Void

public typealias DongleInitResult = (Swift.Result<Any, Error>) -> Void

public final class DongleManager {


    public init() {
    }

    func setupIp(ip:String, port: UInt16) {

        UQDongle.shared.setup(ip: ip, port: port)
    }

    func dongleConnect(_ results: @escaping((Swift.Result<Any, Error>)) -> Void) {

        UQDongle.shared.connect { res in
            switch res {
            case .success(let res):
                results(.success(res))
            case .failure(let err):
                results(.failure(err))

            }
        }
    }

    func dongleSign(amount:Decimal) async throws -> ResSignEx? {
        return try await withCheckedThrowingContinuation { continuation in

            dongleActionSignEx(transactionAmount:NSDecimalNumber(decimal: amount).stringValue) { res in
                switch res {
                case .success(let res):
                    continuation.resume(with: .success(res))
                case .failure(let err):
                    continuation.resume(with: .failure(err))
                }
            }
        }
    }

    func dongleCreditCard(amount:Decimal) async throws -> ResDongleEx? {

        return try await withCheckedThrowingContinuation { continuation in
            dongleActionDongleEx(transactionAmount:NSDecimalNumber(decimal: amount).stringValue) { res in
                switch res {
                case .success(let resOk):
                    continuation.resume(with: .success(resOk))
                case .failure(let err):
                    continuation.resume(with: .failure(err))

                }
            }
        }
    }

    func dongleCashCard(amount:Decimal) async throws -> ResKICAccountPay? {

        return try await withCheckedThrowingContinuation { continuation in

            var needPass:String = "3"
            if abs(amount) > 50000 {
                needPass = "1"
            }
            dongleActionKicAccount(transactionAmount: NSDecimalNumber(decimal: amount).stringValue,
                                   transactionType:needPass) { res in
                switch res {
                case .success(let resOk):
                    continuation.resume(with: .success(resOk))
                case .failure(let err):
                    continuation.resume(with: .failure(err))
                }
            }
        }
    }

    func donglePinNum() async throws -> ResJuminPinEx? {
        return try await withCheckedThrowingContinuation { continuation in

            dongleAction_PinNum(vanCode: "k", title: "은련 비밀번호(6자리)", description: "비밀번호 6자리를 입력하여 주십시오.") { res in
                switch res {
                case .success(let res):
                    continuation.resume(with: .success(res))
                case .failure(let err):
                    continuation.resume(with: .failure(err))
                }
            }
        }
    }

    func dongleReset(_ results: @escaping((Swift.Result<Any, Error>)) -> Void) {
        UQDongle.shared.reqReset{ res in
            switch res {
            case .success(let res):
                print("reset dongle")
                results(.success(res))

            case .failure(let err):
                print("reset fail dongle")
                results(.failure(err))
            }
        }
    }

    func dongleDisconnect() {
        UQDongle.shared.disconnect { res in
            print("Dongle disconnect \(res)")
        }
    }

    func dongleError(err:Error) -> NSError {
        switch err {
        case UQDogleError.dongleFailResponse(let responseCode, let message):
            let nsErr = err as NSError
            return NSError(domain: nsErr.domain,
                           code: dongleErrorCodeMapper(errCode: responseCode),
                           userInfo: ["code":responseCode, "msg":message ?? ""])
//        case UQDogleError.otherCommandBeingExecuted:
        default:
            print("Dongle error::[\(err)]")
        }
        return err as NSError
    }

    func dongleErrorCodeMapper(errCode:UQVerifoneDongle.DongleResponseCode) -> Int {

        var errCd = 0
        switch errCode {
        case .RC_TIMEOUT:
            errCd = 177
        case .RC_FAILURE:
            errCd = 255
        case .RC_AUTH_NOT_PERFORMED:
            errCd = 254
        case .RC_CANCEL:
            errCd = 205
        case .RC_NO_CHIP:
            errCd = 234
        case .RC_NOT_SUPPORT_FC:
            errCd = 154
        case .RC_NO_PIN:
            errCd = 236
        default:
            errCd = 0
        }

        return errCd
    }








    ///------------------------------------------------------------------------
    ///Dongle Action
    ///
    func dongleAction_PinNum(pinLen:String = "6",
                             dispType:Int = 3 ,
                             keyType:Int = 1,
                             inputClassification:Int = 0,
                             vanCode:String,
                             title:String,
                             description:String,
                             results: @escaping(Result<ResJuminPinEx, Error>) -> Void) {


        let param = ReqJuminPinExParameter(nPinLen: UInt(pinLen) ?? 6,
                                           dispType: "\(dispType)",
                                           keyType: "\(keyType)",
                                           inputClassification: "\(inputClassification)",
                                           vanCode: vanCode,
                                           title: title,
                                           description: description)

        UQDongle.shared.reqJuminPinEx(parameter: param, fallback: { data in
            print("Fallback:[\(data.toString() ?? "")]")
        }) { result in
            switch result {
            case .success(let res):
                print("Success pin\(res.description)")
                results(.success(res))
            case .failure(let error):
                print("Error pin\(error.localizedDescription)")
//                var errMsg:String = ""
//                switch error {
//                case UQDogleError.dongleFailResponse(let resCode, let msg):
//                    print("UQDogleError.dongleFailResponse \(resCode) / \(msg)")
//                    if msg != nil {
//                        errMsg =  msg!
//                    } else  {
//                        errMsg = error.localizedDescription
//                    }
//                    let err = PaymentProcessError.DonggleExError(resCode, errMsg)
                    results(.failure(error))

            }
        }
    }

    func dongleActionDongleEx(transactionType:Int = 0,
                              transactionDate:String = Date().yyyyMMddHHmmss,
                              transactionAmount:String,
                              terminalId:String = "00000000",
                              groupCompanyLogo:String = "00",
                              encryptionType:String = "9",
                              serverEncryptionCode:Int = 0,
                              results: @escaping(Result<ResDongleEx, Error>) -> Void) {

        let param = ReqDongleExParameter(transactionType: "\(transactionType)",
                                         transactionDate: transactionDate,
                                                transactionAmount: UInt32(transactionAmount) ?? 0,
                                                terminalId: terminalId,
                                         groupCompanyLogo: groupCompanyLogo,
                                         encryptionType: encryptionType,
                                         serverEncryptionCode: serverEncryptionCode == 0 ? " " : "P")

        UQDongle.shared.reqDongleEx(parameter: param, fallback: { data in

        }) { result in
            switch result {
            case .success(let res):
                results(.success(res))
            case .failure(let error):
                results(.failure(error))
//                print("dongleAction_DongleEx error \(errMsg)")
            }
        }
    }

    func dongleActionKicAccount(transactionAmount:String = "0",
                                transactionType:String = "1",
                                wait:Int = 60,
                                dispType:Int = 3,
                                results: @escaping(Result<ResKICAccountPay, Error>) -> Void) {

        //transactionType : 0x01 : 구매 , 0x02 : 구매환불 , 0x03 : 간소화 구매 , 0x04 : 거래조회 , 0x05 : 잔액조회

        let param = ReqKICAccountPayParameter(wait: UInt16(wait),
                                              transactionType: UInt8(transactionType)!,
                                              transactionAmount: UInt32(transactionAmount) ?? 0,
                                              dispType: "\(dispType)")

        UQDongle.shared.reqKICAccountPay(parameter: param) { result in
            switch result {
            case .success(let res):
                return results(.success(res))
            case .failure(let error):
                return results(.failure(error))
//                print("error-> \(error)")
//                print("error-> \(error.localizedDescription)")
//                let err = PaymentProcessError.DonggleSignError(.RC_FAILURE, error.localizedDescription)
//
            }
        }
    }

    func dongleActionSignEx(transactionAmount:String = "0",
                            timeoutSetting:String = "00",
                            groupCompanyLogo:String = "00",
                            results: @escaping(Result<ResSignEx, Error>) -> Void) {

        let desc:String = "금액: \(transactionAmount)원"
        let signParam = ReqSignExParameter(screenText: desc,
                                           timeoutSetting: "00", //“02” : 2초간 대기 , “00” : 무한정 대기(SIGN완료 전문오면 종료)
                                           groupCompanyLogo: "00")

        UQDongle.shared.reqSignEx(parameter: signParam) { result in

                                        switch result {
                                        case .success(let res):
                                            results(.success(res))
                                            print("success res sign \(res.description)")
                                        case .failure(let error):
//                                            print("fail sign \(error) / \(error.localizedDescription)")
//                                            let err = PaymentProcessError.DonggleSignError(.RC_FAILURE, error.localizedDescription)
                                            results(.failure(error))
                                        }
                                    }

    }

    //-----------------------------------------------------------------------------------------
/*
    func keyUpdateAuto(_ completion: @escaping DongleInitResult) {

        dongleInitKey  { res in
            switch res {
            case .success(let res):
                print("success dongleInitKey \(res)")
                self.dongleLotteVANSecinfo(completion)
            case .failure(let error):
                print("Fail dongleInitKey \(error)")
                completion(.failure(error))
            }
        }
    }

    func dongleInitKey(_ completion: @escaping DongleInitResult) {

        let param:[SetVaninfoData] = [SetVaninfoData(vanAgencyCode: "8", isIntegratedVan: true),
                                      SetVaninfoData(vanAgencyCode: "9", isIntegratedVan: true),
                                      SetVaninfoData(vanAgencyCode: "6", isIntegratedVan: true),
                                      SetVaninfoData(vanAgencyCode: "F", isIntegratedVan: false)]

        UQDongle.shared.setVaninfo(parameter: param, result: { result in
            switch result {
            case .success(_):
                let userDefaults = UserDefaults.standard
                userDefaults.set("", forKey: "")
                self.dongleLotteVANSecinfo(completion)
            case .failure(let error):
                print("setVaninfo fail:\(error.localizedDescription)")
                completion(.failure(error))
            }
        })
    }

    func dongleLotteVANSecinfo(_ completion: @escaping DongleInitResult) {

        let param:ReqLotteVANSecinfoParameter = ReqLotteVANSecinfoParameter.init(keyExchangeClassification: "N", vanAgencyCode: "9")
        print("ReqLotteVANSecinfoParameter \(param)")
        UQDongle.shared.lotteVANSecinfo(parameter: param, result: { result in
            switch result {
            case .success(let res):
                print("dongleLotteVANSecinfo success:\(res.description)")
                self.sendDongleKey(completion)
            case .failure(let error):
                print("dongleLotteVANSecinfo fail.")
                completion(.failure(error))
            }
        })
    }

    func sendDongleKey(_ completion: @escaping DongleInitResult) {

        let param:ReqKeyDownKSN  = ReqKeyDownKSN.init(dictionary: nil)!
        param.header?.msgKind = "37"
        param.header?.seqNo = 1
        param.header?.saleDate = Date().yyyyMMdd
        param.header?.storeNo = storeNum
        param.header?.posNo = posNum
        param.header?.tranNo = 1
        param.header?.respCode = "00"
        param.header?.respMsg = ""

        let dataDic:Dictionary = param.dictionaryRepresentation() as! Dictionary<String, Any>
        let serverApi = PaymentApiTransactionLotte.init(hostUrl!)

        serverApi.sendData("ksn", dicData: dataDic) { result in


            print("resDongleKey::\(result.responseString)")
            if result.result == .successful {

                let resDic = result.responseString.convertToDictionary()
                let resD:ResKeyDownKSN? = ResKeyDownKSN.init(dictionary: resDic as NSDictionary?)
                if resD != nil {
                    self.dongleLotteVanKsnReg(param: (resD?.body)!, completion)
                    return
                }
            }
            let e = NSError(domain: "ksn error", code: 31, userInfo: nil)
            completion(.failure(e))
        }
    }



    func dongleLotteVanKsnReg(param:ResKeyDownKSNBody,_ completion: @escaping DongleInitResult) {

        let dongleParam = ReqLotteVANKSNReqParameter(keyExchangeClassification: "N",
                                                     vanAgencyCode: "9",
                                                     keyClassification: "R",
                                                     publicKeyVersion: "  ",
                                                     serverRandomValue: param.svrRandData!,
                                                     hashValue: param.hashData!,
                                                     rsaAuthenticationValue: param.signData!)

        print("dongleLotteVanksnReg \(dongleParam)")
        UQDongle.shared.lotteVANKSNReq(parameter: dongleParam, result: { result in

            switch result {
            case .success(let res):
                print("dongleLotteVanKsnReg succes \(res.description)")
                self.regDongleKey(encKey: res.encData, ksn: res.ksn, vanACode: res.vanACode, completion)

            case .failure(let error):
                print("dongleLotteVanKsnReg fail:\(error.localizedDescription)")
                completion(.failure(error))
            }
        })
    }




    func regDongleKey(encKey:String, ksn:String, vanACode:String,_ completion: @escaping DongleInitResult) {

        let param:ReqKeyDownIpek  = ReqKeyDownIpek.init(dictionary: nil)!

        param.header?.msgKind = "37"
        param.header?.seqNo = 1
        param.header?.saleDate = Date().yyyyMMdd
        param.header?.storeNo = storeNum
        param.header?.posNo = posNum
        param.header?.tranNo = 1
        param.header?.respCode = "00"
        param.header?.respMsg = ""
        param.header?.respCode = ""

        var encSubKey:String = String(encKey.prefix(4))
        for _ in 0 ..< 40 {
            encSubKey = encSubKey + " "
        }
        print("encK:\(encSubKey.count) :: \(encSubKey)")
        param.body?.indctEncKey = encKey
        param.body?.ksn = ksn

        let dataDic:Dictionary = param.dictionaryRepresentation() as! Dictionary<String, Any>
        let serverApi = PaymentApiTransactionLotte.init(hostUrl!)

        serverApi.sendData("ipek", dicData: dataDic) { result in

            if result.result == .successful {

                let resDic = result.responseString.convertToDictionary()
                let resD:ResKeyDownIpek? = ResKeyDownIpek.init(dictionary: resDic as NSDictionary?)
                if resD != nil {
                    self.dongleLotteVANCryptkeySet(param: (resD?.body)!, completion)
                    return
                }
            }
            let e = NSError(domain: "ipek", code: 31, userInfo: nil)
            completion(.failure(e))
        }

    }



    func dongleLotteVANCryptkeySet(param:ResKeyDownIpekBody,_ completion: @escaping DongleInitResult) {

        let dongleParma = ReqLotteVANCryptKeySetParameter(keyExchangeClassification: "N",
                                                          vanAgencyCode: "9",
                                                          keyClassification: "R",
                                                          ipekKeyGenerationTime: param.keyCreatDate!,
                                                          ipekDataEncryption: param.encData!)

        UQDongle.shared.lotteVANCryptkeySet(parameter: dongleParma) { result in
            switch result {
            case .success(let res):
                print("success dongleLotteVANCryptkeySet \(res.description)")
                completion(.success(res))

            case .failure(let error):
                print("fail dongleLotteVANCryptkeySet \(error.localizedDescription)")
                completion(.failure(error))
            }
        }
    }
*/
}
