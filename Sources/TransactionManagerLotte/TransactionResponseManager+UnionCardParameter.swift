//
//  File.swift
//  
//
//  Created by Administrator on 2022/03/29.
//

import Foundation
import UQVerifoneDongle
import PaymentCartridgeInterfaces

extension TransactionRequestManagerLotte {

    func makeReqUnionCardParamApproval( _ payment:IPayment, param:ReqCreditCard,
                                        resDongleData:ResDongleEx,
                                        paymentCode:PaymentLotteApprovalParameterCode) -> ReqCreditCard {

        param.body?.tranType = PaymentLotteApprovalParameterCode.approval.rawValue
        param.body?.orgAppDate = ""
        param.body?.orgAppNo = ""
        param.body?.cardGb = PaymentLotteParameterCardTyeCode.union.rawValue
        param.body?.saleAmt = NSDecimalNumber(decimal: payment.amount).intValue
        param.body?.vanCode = PaymentLotteParameterVanIdCode.union.rawValue

        param.body?.eCnClNo = (param.header?.saleDate!)! + String(format: "%04d", Int((param.header?.storeNo)!)!  ) + String(format: "%02d", Int((param.header?.posNo)!)! ) + String(format:"%05d",(param.header?.seqNo)!) + String(format:"%04d",(param.header?.tranNo)!)

        if resDongleData.cardSepCode == "0" || resDongleData.cardSepCode == "1" {
            //Pin pad(0:Pin, 1:MS/IC)
            param.body?.wcc = "@"
        } else if resDongleData.cardSepCode == "MS" || resDongleData.cardSepCode == "FB" || resDongleData.cardSepCode == "01" {
            //MS(swipe), FB(swipe fallback), 01(RF)
            param.body?.wcc = "A"
        } else {
            param.body?.wcc = "I"
        }

        if resDongleData.fallbackResCode == "00" {
            param.body?.fallBackType = "Y"
        } else {
            param.body?.fallBackType = "N"
        }
        param.body?.fallBackReason = resDongleData.fallbackRes

        param.body?.cardNo = resDongleData.maskNum
        param.body?.track2 = resDongleData.track2
        param.body?.kSN = resDongleData.ksn

        param.body?.emvLen = "346"//resDongleData.emvDataLen
        var emvString:String = resDongleData.emvDataString
        while ( emvString.count < 400) {
            emvString.append(" ")
        }
        param.body?.emvData = emvString
        param.body?.modelNo = modelNum
        payment.processorTransactionId = param.body?.eCnClNo ?? ""
        payment.accountNumber = param.body?.cardNo ?? ""

        return param
    }


    func makeReqUnionCardParamRefund( _ payment:IPayment, param:ReqCreditCard,
                                      resDongleData:ResDongleEx,
                                      paymentCode:PaymentLotteApprovalParameterCode) -> ReqCreditCard {

        let dicD:NSDictionary = payment.originalPayment?.terminalId?.convertToDictionary() as! NSDictionary
        let resD:ResCreditCard? = ResCreditCard.init(dictionary: dicD as NSDictionary?)
//
        param.body?.saleAmt = NSDecimalNumber(decimal: abs(payment.amount)).intValue
        if NSDecimalNumber(decimal: abs(payment.amount)).intValue != NSDecimalNumber(decimal: abs(payment.originalPayment!.amount)).intValue {
            param.body?.orgSaleAmt = NSDecimalNumber(decimal: abs(payment.originalPayment!.amount)).intValue
        } else {
            param.body?.orgSaleAmt = 0
        }
        param.body?.orgAppDate = resD?.header?.saleDate
        param.body?.orgAppNo = resD?.body?.apprNo
        param.body?.vanCode = resD?.body?.vanCode
        param.body?.cardNo = resDongleData.maskNum
        param.body?.cardGb = PaymentLotteParameterCardTyeCode.union.rawValue
        if resDongleData.cardSepCode == "0" || resDongleData.cardSepCode == "1" {
            //Pin pad(0:Pin, 1:MS/IC)
            param.body?.wcc = "@"
        } else if resDongleData.cardSepCode == "MS" || resDongleData.cardSepCode == "FB" || resDongleData.cardSepCode == "01" {
            //MS(swipe), FB(swipe fallback), 01(RF)
            param.body?.wcc = "A"
        } else {
            param.body?.wcc = "I"
        }
        if resDongleData.fallbackResCode == "00" {
            param.body?.fallBackType = "Y"
            param.body?.fallBackReason = ""
        } else {
            param.body?.fallBackType = "N"
            param.body?.fallBackReason = resDongleData.fallbackRes
        }

        param.body?.cardNo = resDongleData.maskNum
        param.body?.track2 = resDongleData.track2
        param.body?.kSN = resDongleData.ksn
        param.body?.emvData = resDongleData.emvDataString
        param.body?.emvLen = resDongleData.emvDataLen
        param.body?.modelNo = modelNum

//        param.body?.eCnClNo = payment.merchantTransactionId
        param.body?.eCnClNo = (param.header?.saleDate!)! + String(format: "%04d", Int((param.header?.storeNo)!)!  ) + String(format: "%02d", Int((param.header?.posNo)!)! ) + String(format:"%05d",(param.header?.seqNo)!)


        return param
    }

    func makeReqUnionCardParamVoid( _ payment:IPayment, param:ReqCreditCard, paymentCode:PaymentLotteApprovalParameterCode) -> ReqCreditCard {

        let dicD:NSDictionary = payment.originalPayment!.terminalId?.convertToDictionary() as! NSDictionary
        let resD:ResCreditCard? = ResCreditCard.init(dictionary: dicD as NSDictionary?)//
        param.header?.seqNo = Int((resD?.header!.seqNo)!)
        param.body?.tranType = PaymentLotteApprovalParameterCode.void.rawValue

        param.body?.orgAppDate = resD?.header!.saleDate//resD?.header!.saleDate
        param.body?.orgAppNo = resD?.body?.apprNo
        param.body?.orgSaleAmt = 0
        param.body?.saleAmt = NSDecimalNumber(decimal: payment.originalPayment!.amount).intValue
        param.body?.wcc = "@"
        param.body?.vanCode = resD?.body!.vanCode
//        param.body?.cardGb = PaymentLotteParameterCardTyeCode.credit.rawValue
        param.body?.cardGb = PaymentLotteParameterCardTyeCode.union.rawValue
        param.body?.cardNo = ""//resD?.body?.cardNum//"81719999****000002* "
        param.body?.fallBackType = ""
        param.body?.fallBackReason = ""
        param.body?.track2 = ""
        param.body?.kSN = ""
        param.body?.emvData = ""
        param.body?.emvLen = ""
        param.body?.modelNo = modelNum
        param.body?.eCnClNo = payment.originalPayment!.processorTransactionId

        return param
    }

    func makeReqUnionCardParamErrorCancel( _ payment:IPayment, param:ReqCreditCard ,paymentCode:PaymentLotteApprovalParameterCode) -> ReqCreditCard
    {

        param.header?.seqNo = Int(payment.merchantTransactionId)
        param.body?.tranType = PaymentLotteApprovalParameterCode.errorCancel.rawValue

        param.body?.orgAppDate = ""//resD?.header!.saleDate
        param.body?.orgAppDate = ""
        param.body?.orgAppNo = ""
        param.body?.orgSaleAmt = NSDecimalNumber(decimal: payment.originalPayment!.amount).intValue
        param.body?.saleAmt = NSDecimalNumber(decimal: payment.originalPayment!.amount).intValue
        param.body?.wcc = "@"
        param.body?.vanCode = PaymentLotteParameterVanIdCode.kis.rawValue
        param.body?.cardGb = PaymentLotteParameterCardTyeCode.union.rawValue
        param.body?.cardNo = payment.originalPayment!.accountNumber
        param.body?.fallBackType = "N"
        param.body?.fallBackReason = ""
        param.body?.track2 = ""
        param.body?.kSN = ""
        param.body?.emvData = ""
        param.body?.emvLen = ""
        param.body?.modelNo = modelNum
        param.body?.eCnClNo = payment.originalPayment!.processorTransactionId

        return param
    }

    func makeReqUnionCardParam( _ payment:IPayment, _ resDongleData:ResDongleEx?,
                                resPinData:ResJuminPinEx?,
                                paymentCode:PaymentLotteApprovalParameterCode) -> ReqCreditCard {

        //고정 헤더
        var param:ReqCreditCard  = ReqCreditCard.init(dictionary: nil)!
        //변화하지 않는 고정값
        param.header?.msgPath = paramMsgPath
        param.header?.msgKind = paramCreditMsgKind
        param.header?.respCode = ""
        param.header?.respMsg = ""
        param.header?.seqNo = Int(payment.merchantTransactionId)
        param.header?.saleDate = Date().yyyyMMdd
        param.body?.itemId = paramCreditMsgKind

        //포스에서 받는 값
        param.header?.storeNo = storeNum
        param.header?.posNo = posNum
        param.header?.tranNo = Int(payment.receiptNumber ?? "")
        param.body?.divMonth = paramDivMonth

        if paymentCode == .approval {
            param = makeReqUnionCardParamApproval(payment,
                                                  param: param,
                                                   resDongleData: resDongleData!,
                                                   paymentCode: paymentCode)
        } else if paymentCode == .refund {
            param = makeReqUnionCardParamRefund(payment, param: param, resDongleData: resDongleData!, paymentCode: paymentCode)
        } else if paymentCode == .void {
            param = makeReqUnionCardParamVoid(payment, param: param, paymentCode: paymentCode)
        } else if paymentCode == .errorCancel {
            param = makeReqUnionCardParamErrorCancel(payment, param: param, paymentCode: paymentCode)
        }

        if resPinData != nil {
            param.body?.pinNo = resPinData?.enterPin
        } else {
            param.body?.pinNo = ""
        }
//        param.body?.pinNo = "251A37E5CE03C5B6"
        param.body?.pinNo = ""

        payment.processorTransactionId = param.body?.eCnClNo ?? ""
        payment.accountNumber = param.body?.cardNo ?? ""

        return param

    }


}
